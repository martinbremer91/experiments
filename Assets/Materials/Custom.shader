Shader "Unlit/Custom"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct VertexOutput
            {
                float4 clipSpacePos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normal : TEXCOORD1;
                float3 worldPos : TEXCOORD2;
            };

            float4 _Color;

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normal = v.normal;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.clipSpacePos = UnityObjectToClipPos(v.vertex);
                return o;
            }

            float4 frag(VertexOutput o) : SV_Target
            {
                float2 uv = o.uv0;
                float3 normal = o.normal;
                float3 worldPos = o.worldPos;

                // ambient light
                float3 ambientLight = float3(.2, .2, .2);
                
                // direct light
                float3 lightDir = _WorldSpaceLightPos0;
                float3 lightColor = _LightColor0.rgb;
                float lightFalloff = max(0, dot(normal, lightDir));
                float3 directDiffuseLight = lightFalloff.xxx * lightColor;

                // composite
                float3 diffuseLight = ambientLight + directDiffuseLight;
                float3 finalSurfaceColor = _Color * diffuseLight;

                // direct specular light
                float3 camPos = _WorldSpaceCameraPos;
                float3 fragToCam = camPos - worldPos;
                float3 viewDir = normalize(fragToCam);

                return float4(viewDir, 0);
            }
            ENDCG
        }
    }
}
