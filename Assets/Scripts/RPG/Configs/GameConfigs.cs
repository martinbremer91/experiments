using UnityEngine;

[CreateAssetMenu(menuName = "GameSettings", fileName = "GameSettings")]
public class GameConfigs : ScriptableObject {
    public const int AbilitiesNumber = 4;
}
