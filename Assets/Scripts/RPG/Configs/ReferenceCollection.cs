using UnityEngine;

public class ReferenceCollection : MonoBehaviour {

    public static ReferenceCollection Instance;

    public GameConfigs GameConfigs;

    public CharacterStats DefaultCharacterStats;

    private void Awake() {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }
}
