using UnityEngine;

[CreateAssetMenu(menuName = "CharacterStats", fileName = "CharacterStats")]
public class CharacterStats : ScriptableObject {
    [Header("Health")]
    public int MaxHealth;

    [Space(5), Header("Combat")]
    public int BaseACC;
    public int BaseDEF;
    public int BaseSPD;

    [Space(10)]
    public Ability[] Abilities;
}
