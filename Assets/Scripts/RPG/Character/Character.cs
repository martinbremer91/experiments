using System;
using System.Linq;
using UnityEngine;

public class Character : MonoBehaviour, IComparable<Character> {

    public CharacterStats Stats;

    public bool IsPlayer;

    private int health;
    public int Health {
        get => health;
        set {
            if (value < 0)
                health = 0;
            else if (value > Stats.MaxHealth)
                health = Stats.MaxHealth;
            else
                health = value;
        }
    }

    [HideInInspector] public StatusEffect StatusEffects;

    [HideInInspector] public Ability[] Abilities;

    [HideInInspector] public int Initiative;

    private static int initCounter;

    private void Awake() {
        if (Stats == null) {
            Stats = ReferenceCollection.Instance.DefaultCharacterStats;
            Debug.LogWarning("Applied default stats to character: " + gameObject.name, gameObject);
        }

        initCounter++;
    }

    private void Start() {
        Init();
    }

    private void Init() {
        TurnManager.Instance.InitiativeOrder.Add(this);

        Health = Stats.MaxHealth;

        if (Stats.Abilities == null || !Stats.Abilities.Any() || 
            Stats.Abilities.Length > GameConfigs.AbilitiesNumber)
            Debug.LogError("Error in number of abilities in CharacterStats");

        Abilities = new Ability[GameConfigs.AbilitiesNumber];

        for (int i = 0; i < Abilities.Length; i++) {
            Abilities[i] = Stats.Abilities[i];
        }

        initCounter--;

        if (initCounter <= 0)
            TurnManager.Instance.StartRound();
    }

    // Initiative Sorting and Tie-Breaking
    public int CompareTo(Character other) {
        int result = Initiative.CompareTo(other.Initiative) * -1;

        if (result == 0)
            result = IsPlayer && !other.IsPlayer ? -1 : !IsPlayer && other.IsPlayer ? 1 : DiceUtils.FlipCoin();

        return result;
    }
}