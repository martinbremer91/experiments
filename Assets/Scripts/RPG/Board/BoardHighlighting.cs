using UnityEngine;

public class BoardHighlighting : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private SquareBoard board;
    private GameObject highlightQuad;

    [HideInInspector] public float gridOffset;
    private float gridFactor;

    public Vector3 debug;

    private void Update() {
        HighlightBoard();
    }

    public void CreateHighlightQuad(float cellSize) {
        highlightQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
        highlightQuad.name = "Highlight Quad";

        highlightQuad.transform.SetParent(transform);
        highlightQuad.transform.localScale = Vector3.one * cellSize * .01f;
        highlightQuad.transform.localRotation = Quaternion.identity;
        highlightQuad.transform.localPosition = highlightQuad.transform.localPosition + transform.forward * -.1f;

        Destroy(highlightQuad.GetComponent<MeshCollider>());

        highlightQuad.SetActive(false);

        gridFactor = board.gridCellsPerAxis / (board.textureSize * .01f);
    }

    private void HighlightBoard() {
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector3.forward, out hit);

        bool hitBoard = hit.collider != null;
        highlightQuad.SetActive(hitBoard);

        Cursor.visible = !hitBoard;

        if (!hitBoard)
            return;

        SnapHighlightQuadToBoardGrid(hit);
    }

    private void SnapHighlightQuadToBoardGrid(RaycastHit hit) {
        Vector3 hitInLocalSpace = transform.InverseTransformPoint(hit.point) +
            new Vector3(gridOffset, gridOffset, 0);

        hitInLocalSpace = new Vector3(hitInLocalSpace.x * gridFactor, hitInLocalSpace.y * gridFactor, 0);

        int xCoord = Mathf.Clamp(Mathf.RoundToInt(hitInLocalSpace.x), 0, board.gridCellsPerAxis - 1);
        int yCoord = Mathf.Clamp(Mathf.RoundToInt(hitInLocalSpace.y), 0, board.gridCellsPerAxis - 1);

        debug = hitInLocalSpace;

        highlightQuad.transform.localPosition = board.cellCoordinates[xCoord, yCoord];
        highlightQuad.transform.localPosition += transform.forward * -.01f;
    }
}
