using UnityEngine;
using UnityEditor;

public class SquareBoard : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    private Sprite boardSprite;

    public int gridCellsPerAxis;
    [HideInInspector] public Vector2[,] cellCoordinates;
    [HideInInspector] public float cellSize;
    
    [HideInInspector] public int textureSize;

    [SerializeField] private bool displayDebugQuads;

    private void Awake() {
        GenerateBoard();
    }

    public void GenerateBoard() {
        if (boardSprite != null) {
            spriteRenderer.sprite = null;
            Destroy(boardSprite);
        }

        textureSize = 700;

        Texture2D texture = new Texture2D(textureSize, textureSize);
        Rect rect = new Rect(transform.position, Vector2.one * textureSize);
        
        boardSprite = Sprite.Create(texture, rect, Vector2.one * .5f);
        boardSprite.name = "BoardSprite";

        spriteRenderer.sprite = boardSprite;

        gameObject.AddComponent<BoxCollider>();

        cellCoordinates = new Vector2[gridCellsPerAxis, gridCellsPerAxis];
        cellSize = textureSize / gridCellsPerAxis;

        BoardHighlighting highlighting = GetComponent<BoardHighlighting>();
        highlighting.CreateHighlightQuad(cellSize);
        highlighting.gridOffset = textureSize * .5f * .01f;

        for (int x = 0; x < gridCellsPerAxis; x++) {
            for (int y = 0; y < gridCellsPerAxis; y++) {
                float xCoord = -textureSize * .5f + x * cellSize + cellSize * .5f;
                float yCoord = -textureSize * .5f + y * cellSize + cellSize * .5f;
                
                cellCoordinates[x, y] = new Vector2(xCoord * .01f, yCoord * .01f);

                if (displayDebugQuads) {
                    GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    quad.transform.SetParent(transform);
                    quad.transform.localScale = Vector3.one * cellSize * .01f;
                    quad.transform.localRotation = Quaternion.identity;
                    quad.transform.localPosition = cellCoordinates[x, y];
                    quad.transform.localPosition = quad.transform.localPosition + new Vector3(0, 0, -.1f);
                }
            }
        }
    }
}