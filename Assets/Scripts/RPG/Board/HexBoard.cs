using UnityEngine;
using TMPro;

public class HexBoard : MonoBehaviour
{
    [SerializeField] private SpriteRenderer hexPrefab;

    private HexCell[,] hexGrid;

    [SerializeField] private Vector2Int gridSize;
    
    // Radii of hex from center to corner and center to edge
    [SerializeField] private float hexRadiusCorner;
    private float hexRadiusEdge;

    private void Awake() {
        hexRadiusEdge = hexRadiusCorner * (Mathf.Sqrt(3) * .5f);

        if (gridSize.x > gridSize.y)
            Debug.LogWarning("Does not currently support hex grids with greater widths than heights. Grid will generate upright.");

        GenerateHexGrid();
    }

    private void GenerateHexGrid() {
        int gridSizeMax = gridSize.x >= gridSize.y ? gridSize.x : gridSize.y;
        int gridSizeMin = gridSize.x >= gridSize.y ? gridSize.y : gridSize.x;
        
        hexGrid = new HexCell[gridSizeMax, gridSizeMax];

        int westSkipCount = 0;
        int eastSkipCount = 0;

        for (int y = 0; y < gridSizeMax; y++) { 
            for (int x = 0; x < gridSizeMax; x++) {

                if (x > gridSizeMin - 1 + eastSkipCount)
                    continue;

                if (y > gridSizeMin - 1 && x <= westSkipCount)
                    continue;

                SpriteRenderer spriteRend = Instantiate(hexPrefab, transform);

                Vector3 pos = new Vector3(
                    1.5f * x * hexRadiusCorner - 1.5f * y * hexRadiusCorner, 
                    x * hexRadiusEdge + y * hexRadiusEdge, 
                    0);

                spriteRend.transform.localPosition = pos;

                HexCell hexCell = new HexCell(spriteRend, new Vector2Int(x, y));
                hexCell.TextObj.text = x + "/" + y;
            }

            if (y > gridSizeMin - 1)
                westSkipCount++;

            eastSkipCount++;
        }
    }
}

public class HexCell {
    public Vector2Int Coords;
    public SpriteRenderer SpriteRend;

    public TMP_Text TextObj;

    public HexCell(SpriteRenderer spriteRend, Vector2Int coords) {
        SpriteRend = spriteRend;
        Coords = coords;

        TextObj = spriteRend.GetComponentInChildren<TMP_Text>();
    }
}