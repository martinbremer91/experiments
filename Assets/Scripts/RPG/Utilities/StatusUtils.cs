using System;

public static class StatusUtils {
    
}

[Flags]
public enum StatusEffect {
    None = 0,
    Bleed = 1,
    Blight = 2
}
