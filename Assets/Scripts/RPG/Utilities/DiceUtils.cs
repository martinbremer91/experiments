using UnityEngine;

public static class DiceUtils {
    public static int MaxValue(DiceType diceType) {
        switch (diceType) {
            case DiceType.D2:
                return 2;
            case DiceType.D4:
                return 4;
            case DiceType.D6:
                return 6;
            case DiceType.D8:
                return 8;
            case DiceType.D10:
                return 10;
            case DiceType.D12:
                return 12;
            case DiceType.D20:
                return 20;
            case DiceType.D100:
                return 100;
            default:
                throw new System.Exception("Invalid Dice Type");
        }
    }

    public static int Roll(DiceType diceType) {
        return Random.Range(1, MaxValue(diceType) + 1);
    }

    public static int Roll(params DiceRoll[] diceRolls) {
        int result = 0;
        foreach (DiceRoll dr in diceRolls) {
            for (int i = 0; i < dr.Amount; i++) {
                result += Random.Range(1, MaxValue(dr.DiceType) + 1);
            }
        }

        return result;
    }

    public static int FlipCoin() {
        return UnityEngine.Random.Range(0, 2) == 0 ? -1 : 1;
    }
}

public enum DiceType {
    D2 = 0,
    D4 = 1,
    D6 = 2,
    D8 = 4,
    D10 = 8,
    D12 = 16,
    D20 = 32,
    D100 = 64
}

public struct DiceRoll {
    public DiceType DiceType;
    public int Amount;

    public DiceRoll(DiceType diceType, int amount) {
        DiceType = diceType;
        Amount = amount;
    }
}
