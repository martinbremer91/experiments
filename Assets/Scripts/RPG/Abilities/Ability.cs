using UnityEngine;
using System.Collections.Generic;

public class Ability : MonoBehaviour {
    
    public string Title;

    [SerializeField] private List<AbilityEffect> AbilityEffects;
    
    public void Execute() {
        foreach (AbilityEffect effect in AbilityEffects)
            effect.ExecuteEffect();
    }
}
