using UnityEngine;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour
{
    public Button Button;
    public Text AbilityTitleText;

    private Ability currentAbility;
    public Ability CurrentAbility {
        get => currentAbility;
        set {
            Button.enabled = value != null;
            currentAbility = value;
        }
    }

    public void SetAbilityButton(Ability ability) {
        CurrentAbility = ability;
        
        if (currentAbility == null)
            return;

        AbilityTitleText.text = ability.Title;

        Button.onClick.RemoveAllListeners();
        Button.onClick.AddListener(CurrentAbility.Execute);
    }
}