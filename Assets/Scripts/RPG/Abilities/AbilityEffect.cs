using UnityEngine;

public class AbilityEffect : MonoBehaviour {
    public virtual void ExecuteEffect() {
        SoundManager.Instance.PlaySound(AudioCategory.UI);
    }
}
