using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    [SerializeField] private AudioSource uiAudioSource;
    [SerializeField] private AudioSource musicAudioSource;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else if (Instance != this)
            Destroy(gameObject);
    }

    public void PlaySound(AudioCategory category) {
        GetAudioSourceByCategory(category).Play();
    }

    private AudioSource GetAudioSourceByCategory(AudioCategory category) {
        switch (category) {
            case AudioCategory.Music:
                return musicAudioSource;
            case AudioCategory.UI:
                return uiAudioSource;
            default:
                throw new System.Exception("Invalid audio category");
        }
    }
}

public enum AudioCategory {
    UI,
    Music
}