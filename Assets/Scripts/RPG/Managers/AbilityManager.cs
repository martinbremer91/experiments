using UnityEngine;
using System.Collections.Generic;

public class AbilityManager : MonoBehaviour {

    public static AbilityManager Instance;

    [SerializeField] private AbilityButton abilityButtonPrefab;

    [HideInInspector] public List<AbilityButton> AbilityButtons;

    private void Awake() {
        if (Instance == null)
            Instance = this;
        else if (Instance != this) {
            Destroy(gameObject);
            return;
        }

        Init();
    }

    private void Init() {
        for (int i = 0; i < GameConfigs.AbilitiesNumber; i++) {
            AbilityButtons.Add(Instantiate(abilityButtonPrefab, transform));
        }
    }
}
