using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TurnManager : MonoBehaviour {

    public static TurnManager Instance;

    [HideInInspector] public List<Character> InitiativeOrder;

    public static Character ActiveCharacter;

    public static List<Character> ActiveTargets;

    private void Awake() {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    public void StartRound() {
        RollInitiative();
        ActiveCharacter = InitiativeOrder[0];
        StartTurn();
    }

    private void StartTurn() {
        for (int i = 0; i < AbilityManager.Instance.AbilityButtons.Count; i++) {
            AbilityManager.Instance.AbilityButtons[i].SetAbilityButton(ActiveCharacter.Abilities[i]);
        }
    }

    public void RollInitiative() {
        InitiativeOrder.ForEach(c => c.Initiative = DiceUtils.Roll(DiceType.D100) + c.Stats.BaseSPD);
        InitiativeOrder.Sort();
    }
}